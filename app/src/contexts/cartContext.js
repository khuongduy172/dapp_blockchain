import React, { createContext, useReducer, useContext, useEffect } from 'react';

const CartContext = createContext();

const initialState = {
  cart: [],
};

export const getCartTotal = (cart) => 
  cart?.reduce((amount, item) => item.price + amount, 0);

const ADD_TO_CART = 'addToCart';
const EMPTY_CART = 'emptyCart';
const REMOVE_FROM_CART = "removeFromCart";

export const action = {
  addToCart: (payload) => {
    return {
        type: ADD_TO_CART,
        payload,
    }
  },
  emptyCart: (payload) => {
    return {
        type: EMPTY_CART,
        payload,
    }
  },
  removeFromCart: (payload) => {
    return {
        type: REMOVE_FROM_CART,
        payload,
    }
  }
}

function reducer(state = initialState, action) {
  switch (action.type) {
    case ADD_TO_CART:
      return {
        ...state,
        cart: [...state.cart, action.payload],
      };

    case EMPTY_CART:
      return {
        ...state,
        cart: [],
      };

    case REMOVE_FROM_CART:
      const index = state.cart.findIndex(
        (cartItem) => cartItem.id === action.id
      );
      let newCart = [...state.cart];

      if (index >= 0) {
        newCart.splice(index, 1);
      } else {
        console.warn(
          `Cant remove product (id: ${action.id}) as its not in cart!`
        );
      }
      return {
        ...state,
        cart: newCart,
      };
    default:
      return {
        ...state
      }
  }
}

export const CartProvider = ({ children }) => {
  const [state, dispatch] = useReducer(reducer, initialState);

  return (
    <CartContext.Provider value={[state, dispatch]}>{children}</CartContext.Provider>
  );
}

export const useCartStore = () => {
  const [state, dispatch] = useContext(CartContext);
  return [state, dispatch];
};
