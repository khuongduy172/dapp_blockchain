import React, { createContext, useReducer, useContext } from 'react';

const ContractContext = createContext();

const initState = {
    account: null,
    nft: {},
    marketplace: {},
    searchInput: '',
};

const SETACCOUNT = "setAccount"
const SETNFT = "setNft"
const SETMARKETPLACE = 'setMarketplace';
const SETSEARCHINPUT = 'setSearchInput';

const setNft = (payload) => {
        return {
            type: SETNFT,
            payload,
        };
    }

const setAccount = (payload) => {
        return {
            type: SETACCOUNT,
            payload,
        };
    }

const setMarketplace = (payload) => {
  return {
    type: SETMARKETPLACE,
    payload,
  };
};

const setSearchInput = (payload) => {
    return {
        type: SETSEARCHINPUT,
        payload,
    };
}

export const action = {
    setAccount,
    setNft,
    setMarketplace,
    setSearchInput,
};

function reducer(state, action) {
    switch (action.type) {
        case SETACCOUNT:
            return {...state, account: action.payload}
        case SETNFT: 
            return {...state, nft: action.payload}
        case SETMARKETPLACE: 
            return {...state, marketplace: action.payload}
        case SETSEARCHINPUT:
            return {...state, searchInput: action.payload}
        default:
            throw new Error('Invalid action');
    }
}

export function ContractProvider({ children }) {
  const [state, dispatch] = useReducer(reducer, initState);

  return (
    <ContractContext.Provider value={[state, dispatch]}>{children}</ContractContext.Provider>
  );
}


export const useStore = () => {
    const [state, dispatch] = useContext(ContractContext);
    return [state, dispatch];
};