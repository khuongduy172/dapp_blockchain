import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import './index.css';
import * as serviceWorker from './serviceWorker';
import { BrowserRouter } from "react-router-dom";
import { ContractProvider } from './contexts/contractContext';
import { CartProvider } from './contexts/cartContext';

ReactDOM.render(
    <BrowserRouter>
        <ContractProvider>
            <CartProvider>
                <App />
            </CartProvider>
        </ContractProvider>
    </BrowserRouter>, 
document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
