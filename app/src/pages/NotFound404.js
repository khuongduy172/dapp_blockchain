import React from 'react'

function NotFound404() {
  return (
    <h1>The page you are looking for does not exist</h1>
  )
}

export default NotFound404