import React, { useEffect, useState } from 'react';
import ListGroup from 'react-bootstrap/ListGroup';
import Breadcrumb from 'react-bootstrap/Breadcrumb';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import CartItem from '../components/CartItem';
import Button from 'react-bootstrap/Button';
import classes from './Cart.module.css';
import Layout from '../layouts/Layout';
import {Link} from 'react-router-dom';

function Cart({ marketplace, nft, web3Handler, account, loading }) {
  const [skinItems, setSkinItems] = useState();

  useEffect(() => {
    setSkinItems(
      localStorage.getItem('cart')
        ? JSON.parse(localStorage.getItem('cart'))
        : []
    );
  }, [JSON.parse(localStorage.getItem('cart'))?.length]);

  return (
    <Layout web3Handler={web3Handler} account={account}>
      <div style={{ backgroundColor: '#001C35', minHeight: '420px' }}>
        <Breadcrumb>
          <Breadcrumb.Item href="/store">Cửa hàng</Breadcrumb.Item>
          <Breadcrumb.Item active>Giỏ hàng của bạn</Breadcrumb.Item>
        </Breadcrumb>
        <Row style={{ padding: '0 15%', width: '100%' }}>
          {skinItems?.map((item, i) => (
            <CartItem
              key={i}
              totalPrice={item.totalPrice}
              itemId={item.itemId}
              skinName={item.skinName}
              description={item.description}
              img={item.img}
              number={i}
              marketplace={marketplace}
              item={item}
            />
          ))}
        </Row>

        <Row style={{ padding: '0 15%', width: '100%' }}>
          <Col style={{ padding: '0' }}>
            <Button>XÓA HẾT</Button>
          </Col>
          <Col style={{ textAlign: 'right', padding: '0' }}>
            <Button>
              <Link
                to="/store"
                style={{ textDecoration: 'none', color: '#fff' }}
              >
                TIẾP TỤC MUA HÀNG
              </Link>
            </Button>
          </Col>
        </Row>
      </div>
    </Layout>
  );
}

export default Cart;
