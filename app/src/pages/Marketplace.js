import React from 'react';
import classes from './Marketplace.module.css';
import logo from '../assets/images/—Pngtree—logo elang pembohong dengan warna_7266610 1.png';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faStar } from '@fortawesome/free-solid-svg-icons';
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import ListGroup from 'react-bootstrap/ListGroup';
import Badge from 'react-bootstrap/Badge';
import ProgressBar from 'react-bootstrap/ProgressBar';
import img from '../assets/images/Rectangle 273.png';
import Product from '../components/Product';
import { Switch, Route, Link, useRouteMatch } from 'react-router-dom';
import SoldItems from './SoldItems';
import SellingItems from './SellingItems';
import MyStore from './MyStore';


function Marketplace({account_name, email, phone}) {

  let { path, url } = useRouteMatch();

  return (
    <div className="mkp" style={{backgroundColor: "#001C35", display: "flow-root"}}>
      
      <div className={classes.mkp__logo}>
        <h1 className={classes.mkp__title_h1}>LOLMarketplace</h1>
        <div className={classes.mkp__line}></div>
        <div className={classes.mkp__circle}>
          <img src={logo} alt="logo" className={classes.mkp__img}/>
        </div>
        <Navbar variant="dark" className={classes.mkp__nav}>
          <Container>
            <Nav className="me-auto">
              <Nav.Link className={classes.mkp__nav_item}><Link to={`${url}/sold`}>Đã bán</Link></Nav.Link>
              <Nav.Link className={classes.mkp__nav_item}><Link to={`${url}/selling`}>Đang bán</Link></Nav.Link>
              <Nav.Link className={classes.mkp__nav_item}><Link to={`${url}/my-store`}>Kho hàng</Link></Nav.Link>
            </Nav>
          </Container>
        </Navbar>
      </div>

      <Container className={classes.mkp__container}>
        <Row>
          <Col style={{backgroundColor: "#71B7EA"}}>
            <ListGroup variant='flush'>
              <ListGroup.Item className={classes.remove_border} style={{backgroundColor: "#71B7EA"}}>
                <h4 style={{textAlign: "center"}}>Tài khoản</h4>
                <Row>
                  <Col>
                    <p>Account</p>
                    <p>Email</p>
                    <p>Điện thoại</p>
                  </Col>
                  <Col>
                    <p>: {account_name}</p>
                    <p>: {email}</p>
                    <p>: {phone}</p>
                  </Col>
                </Row>

                <div className={classes.mkp__abt}>
                  <div className={classes.mkp__abt_website}>Về website</div>
                  <div className={classes.mkp__abt_decors}>
                    <div className={classes.mkp__abt_line}></div>
                    {Array(4).fill().map((_, i) => (
                      <FontAwesomeIcon key={i} icon={faStar} style={{color: "orange"}}/>
                    ))}
                    <div className={classes.mkp__abt_line}></div>
                  </div>
                </div>
              </ListGroup.Item>
              <ListGroup.Item className={`${classes.remove_border} ${classes.padding_left_0}`} style={{backgroundColor: "#71B7EA"}}>
                <Badge bg="dark" className={classes.badge_style}>Doanh thu</Badge>

                <Container style={{padding: "50px 0", height: "180px"}} className={classes.prog_container}>
                  <ProgressBar variant="warning" now={80} />
                  <ProgressBar variant="danger" now={60} />
                  <ProgressBar variant="primary" now={70} />
                </Container>

                <Container>
                  <Row>
                    <Col><span className={classes.prog_dot} style={{backgroundColor: "orange"}}></span>{' '} 2019</Col>
                    <Col><span className={classes.prog_dot} style={{backgroundColor: "red"}}></span>{' '} 2020</Col>
                    <Col><span className={classes.prog_dot} style={{backgroundColor: "blue"}}></span>{' '} 2021</Col>
                  </Row>
                </Container>
              </ListGroup.Item>
              <ListGroup.Item className={`${classes.remove_border} ${classes.padding_left_0}`} style={{backgroundColor: "#71B7EA"}}>
                <Badge bg="dark" className={classes.badge_style}>Top tiêu thụ</Badge>  

                <Container style={{padding: "50px 0", height: "250px"}} className={classes.prog_container}>
                  <Row className={classes.alg_item_cen}>
                    <Col><span className={classes.font_12}>Skin Yasuo</span></Col>
                    <Col xs={8}><ProgressBar striped animated variant="dark" now={30} /></Col>
                  </Row>
                  <Row className={classes.alg_item_cen}>
                    <Col><span className={classes.font_12}>Đi rừng</span></Col>
                    <Col xs={8}><ProgressBar striped animated variant="dark" now={70} /></Col>
                  </Row>
                  <Row className={classes.alg_item_cen}>
                    <Col><span className={classes.font_12}>Skin Lee Sin</span></Col>
                    <Col xs={8}><ProgressBar striped animated variant="dark" now={50} /></Col>
                  </Row>
                  <Row className={classes.alg_item_cen}>
                    <Col><span className={classes.font_12}>Skin Yasuo</span></Col>
                    <Col xs={8}><ProgressBar striped animated variant="dark" now={90} /></Col>
                  </Row>
                  
                  
                  
                </Container>
              </ListGroup.Item>
            </ListGroup>
          </Col>

          <Col xs={9}>
              <Switch>
                <Route exact path={path}><SoldItems /></Route>
                <Route path={`${path}/:items`}><SoldItems /></Route>            
              </Switch>
          
          </Col>
        </Row>
      </Container>
    </div>
  )
}

export default Marketplace