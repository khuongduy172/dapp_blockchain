import classes from '../Marketplace.module.css';
import classes2 from './ProductManagement.module.css';
import React, { useState, useEffect } from 'react';
import AdminLayout from '../../layouts/AdminLayout';
import { Form, Image, Row, Col, Button, Container, Modal, Spinner } from 'react-bootstrap';
import {client} from '../../utilities/ipfs';
import {useStore} from '../../contexts/contractContext';
import { useHistory } from 'react-router-dom';
import { ethers } from 'ethers';

function ProductManagement({ marketplace, nft, web3Handler, account, loading }) {
  const [img, setImg] = useState('');
  const [success, setSuccess] = useState(false);
  const [code, setCode] = useState('');
  const [chamName, setChamName] = useState('');
  const [skinName, setSkinName] = useState('');
  const [year, setYear] = useState();
  const [price, setPrice] = useState('');
  const [description, setDescription] = useState('');
  const history = useHistory();

  useEffect(async () => {
    if (!account) {
      history.push('/home');
    } else {
      const acc = await marketplace.feeAccount();
      if (account.toLowerCase() !== acc.toLowerCase()) {
        history.push('/home');
      }
    }
  }, [account]);

  function randomString(length, chars) {
    let result = '';
    for (let i = length; i > 0; --i)
      result += chars[Math.floor(Math.random() * chars.length)];
    return result;
  }
  const generateCode = () => {
    let rString = randomString(16, '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ');
    setCode(rString);
  };

  const uploadToIPFS = async (file) => {
    if (typeof file !== 'undefined') {
      try {
        const result = await client.add(file);
        // console.log(result);
        setImg(`http://127.0.0.1:8080/ipfs/${result.path}`);
      } catch (error) {
        console.log('ipfs image upload error: ', error);
      }
    }
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    // if (!img || !price || !chamName || !skinName || !year) return;
    try {
      const result = await client.add(
        JSON.stringify({ img, chamName, skinName, year, description, price })
      );
      mintThenList(result);
    } catch (error) {
      console.log('ipfs uri upload error: ', error);
    }
  };

  const mintThenList = async (result) => {
    const uri = `http://127.0.0.1:8080/ipfs/${result.path}`;
    // mint nft
    await (await nft.mint(uri, code)).wait();
    // get tokenId of new nft
    const id = await nft.tokenCount();
    // approve marketplace to spend nft
    await (await nft.setApprovalForAll(marketplace.address, true)).wait();
    // add nft to marketplace
    const listingPrice = ethers.utils.parseEther(price.toString());
    await (await marketplace.makeItem(nft.address, id, listingPrice)).wait();
    setSuccess(true)
  };

  return (
    <AdminLayout web3Handler={web3Handler} account={account}>
      {loading ? (
        <Spinner animation="border" variant="primary" />
      ) : (
        <Container>
          <Modal
            show={success}
            onHide={() => {
              setSuccess(false);
            }}
          >
            <Modal.Header closeButton>
              <Modal.Title>Thông báo</Modal.Title>
            </Modal.Header>
            <Modal.Body>Thêm thành công</Modal.Body>
            <Modal.Footer>
              <Button
                variant="secondary"
                onClick={() => {
                  setSuccess(false);
                }}
              >
                Đóng
              </Button>
            </Modal.Footer>
          </Modal>
          <Form onSubmit={handleSubmit}>
            <h2 className="mt-3">THÊM SẢN PHẨM</h2>
            <Row className="g-2">
              <Col md>
                <Form.Group className="mb-3 p" controlId="ten-tuong">
                  <Form.Label>Tên tướng</Form.Label>
                  <Form.Control
                    type="text"
                    required
                    placeholder="Nhập tên tướng..."
                    value={chamName}
                    onChange={(e) => {
                      setChamName(e.target.value);
                    }}
                  />
                </Form.Group>
              </Col>
              <Col md>
                <Form.Group className="mb-3 p" controlId="ten-trang-phuc">
                  <Form.Label>Tên trang phục</Form.Label>
                  <Form.Control
                    type="text"
                    placeholder="Nhập tên trang phục..."
                    required
                    value={skinName}
                    onChange={(e) => {
                      setSkinName(e.target.value);
                    }}
                  />
                </Form.Group>
              </Col>
            </Row>
            <Row className="g-2">
              <Col md>
                <Form.Group className="mb-3 p" controlId="nam-phat-hanh">
                  <Form.Label>Năm phát hành</Form.Label>
                  <Form.Control
                    type="number"
                    placeholder="Nhập năm phát hành..."
                    required
                    value={year}
                    onChange={(e) => {
                      setYear(e.target.value);
                    }}
                  />
                </Form.Group>
              </Col>
              <Col md>
                <Form.Group className="mb-3 p" controlId="gia-tien">
                  <Form.Label>Giá tiền</Form.Label>
                  <Form.Control
                    type="number"
                    required
                    value={price}
                    onChange={(e) => {
                      setPrice(e.target.value);
                    }}
                  />
                </Form.Group>
              </Col>
            </Row>
            <Form.Group className="mb-3 p" controlId="mo-ta">
              <Form.Label>Mô tả</Form.Label>
              <Form.Control
                as={'textarea'}
                placeholder="Nhập mô tả..."
                value={description}
                onChange={(e) => {
                  setDescription(e.target.value);
                }}
              />
            </Form.Group>
            <Row className="g-2">
              <Col md>
                <Form.Group className="mb-3 p" controlId="ma-code">
                  <Form.Label>Mã code</Form.Label>
                  <Form.Control type="text" disabled value={code} required />
                  <Button className="btn btn-secondary" onClick={generateCode}>
                    Tạo mã code
                  </Button>
                </Form.Group>
              </Col>
              <Col md>
                <Form.Group className="mb-3 p" controlId="hinh-anh">
                  <Form.Label>Hình ảnh</Form.Label>
                  <Form.Control
                    type="file"
                    onChange={(e) => uploadToIPFS(e.target.files[0])}
                    required
                  />
                </Form.Group>
              </Col>
            </Row>
            <div className="mb-3">
              {img !== '' && (
                <Image
                  className={classes2.product_management_create__img}
                  src={img}
                  alt=""
                />
              )}
            </div>
            <Button variant="primary" type="submit">
              Đăng
            </Button>
          </Form>
        </Container>
      )}
    </AdminLayout>
  );
}

export default ProductManagement;
