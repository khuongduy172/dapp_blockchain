import React, { useState, useEffect, useRef }  from 'react'
import Layout from '../../layouts/Layout.js'
import ProjectYasuo from '../../assets/project-yasuo.svg'
import productDetail from './ProductDetail.module.css'
import ProductSimilar from '../../components/ProductSimilar'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { solid, regular, brands } from '@fortawesome/fontawesome-svg-core/import.macro';
import { useParams } from 'react-router-dom';
import {BigNumber, ethers} from 'ethers';
import { Spinner } from 'react-bootstrap';


function DetailSub ({marketplace, nft}) {
  const { skinID } = useParams();
  const [product, setProduct] = useState();

  const [num, setNum] = useState(0);
  const [trans, setTrans] = useState();
  const move = useRef();
  const skin = useRef();

  useEffect(() => {
    setTrans(-1150 * num);
  }, [num]);

  
  useEffect(async () => {
    const pdt = await marketplace.items(BigNumber.from(skinID));
    const uri = await nft.tokenURI(pdt.tokenId);
    // use uri to fetch the nft metadata stored on ipfs
    const response = await fetch(uri);
    const metadata = await response.json();
    // get total price of item (item price + fee)
    const totalPrice = await marketplace.getTotalPrice(pdt.itemId);
    const pro = {
      totalPrice,
      itemId: pdt.itemId,
      seller: pdt.seller,
      chamName: metadata.chamName,
      skinName: metadata.skinName,
      description: metadata.description,
      img: metadata.img,
      year: metadata.year,
    };

    setProduct(pro);
  }, []);

  const handleRightClick = () => {
    if (num < Math.floor(move.current.childNodes.length / 4))
      setNum((num) => num + 1);
    // console.log(num, "click");
  };

  const handleLeftClick = () => {
    if (num > 0) setNum((num) => num - 1);
    // console.log(num);
  };
  if(!product) {
    return <Spinner animation="border" variant="primary" />;
  }
  return (
    <div className={productDetail.product_detail}>
          <div className={productDetail.product_detail__nav}>
            Cửa hàng {">"}{" "}
            <span style={{ color: "#FAFF03" }}>Trang phục {product.skinName}</span>
          </div>

          <div className={productDetail.product_detail__wrapper}>
            <div className={productDetail.product_detail__wrapper__img}>
              <img src={product.img} alt='skin'  style={{objectFit: "contain", width: "100%"}}/>
            </div>

            <div className={productDetail.product_detail__wrapper_info}>
              <div className={productDetail.product_detail__wrapper_info_name}>
                {product.skinName}
              </div>
              <div className={productDetail.product_detail__wrapper_info_release}>
                Năm ra mắt: {product.year}
              </div>
              <div className={productDetail.product_detail__wrapper_info_price}>
                Giá:{" "}
                <span
                  className={
                    productDetail.product_detail__wrapper_info_price__color
                  }
                >
                {ethers.utils.formatEther(product.totalPrice)} ETH
                </span>
              </div>

              <div className={productDetail.product_detail__wrapper_info_button}>
                <button
                  className={
                    productDetail.product_detail__wrapper_info_button__addCart
                  }
                >
                  Thêm vào giỏ hàng
                </button>
                <button
                  className={
                    productDetail.product_detail__wrapper_info_button__sendGift
                  }
                >
                  Tặng quà
                </button>
              </div>
            </div>
          </div>

          <div className={productDetail.product_detail_desc}>
            <div className={productDetail.product_detail_desc__title}>MÔ TẢ</div>
            <div className={productDetail.product_detail_desc__content}>
              {product.description}
            </div>
          </div>

          <div className={productDetail.product_detail__wrapper}>
            <FontAwesomeIcon
              className={productDetail.product_detail__icon}
              icon={solid("angle-left")}
              onClick={handleLeftClick}
            />

            {/* <div className={productDetail.product_detail__more}>
              <ul
                className={productDetail.product_detail__more_list}
                style={{ left: trans + "px" }}
                ref={move}
              >
                {Array(6).fill().map((item, index) => (
                    <ProductSimilar
                      index={index}
                      ref={product}
                      name={item.name}
                      price={item.price}
                      image={item.image}
                      state={item.state}
                    />
                ))}
              </ul>
            </div> */}

            <FontAwesomeIcon
              className={productDetail.product_detail__icon}
              icon={solid("angle-right")}
              onClick={handleRightClick}
            />
          </div>
        </div>
  )
}

function ProductDetail({marketplace, nft, web3Handler, account, loading}) {
  return (
    <Layout account={account} web3Handler={web3Handler}>
      {loading ? (
        <Spinner animation="border" variant="primary" />
      ) : (
        <DetailSub marketplace={marketplace} nft={nft} />
      )}
    </Layout>
  );
}

export default ProductDetail