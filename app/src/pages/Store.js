import React, {useState, useEffect } from 'react';
import Layout from '../layouts/Layout';
import classes from './Store.module.css';
import { Container, Row, Col, ListGroup, Form, Accordion, Pagination, Spinner } from 'react-bootstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faStar } from '@fortawesome/free-solid-svg-icons';
import Product from '../components/Product';
import img from '../assets/images/Rectangle 273.png';
import {useStore, action} from '../contexts/contractContext';
import {useCartStore} from '../contexts/cartContext';

function Sub({ marketplace, nft }) {
  const [items, setItems] = useState([]);
  const loadMarketplaceItems = async () => {
    // Load all unsold items
    const itemCount = await marketplace.itemCount();
    let items = [];
    for (let i = 1; i <= itemCount; i++) {
      const item = await marketplace.items(i);
      const active = await nft.active(item.tokenId);
      if (!item.sold && !active) {
        // get uri url from nft contract
        const uri = await nft.tokenURI(item.tokenId);
        // use uri to fetch the nft metadata stored on ipfs
        const response = await fetch(uri);
        const metadata = await response.json();
        // get total price of item (item price + fee)
        const totalPrice = await marketplace.getTotalPrice(item.itemId);
        // Add item to items array
        items.push({
          totalPrice,
          itemId: item.itemId,
          seller: item.seller,
          chamName: metadata.chamName,
          skinName: metadata.skinName,
          description: metadata.description,
          img: metadata.img,
          year: metadata.year,
        });
      }
    }
    setItems(items);
  };

  const buyMarketItem = async (item) => {
    await (
      await marketplace.purchaseItem(item.itemId, {
        value: item.totalPrice,
      })
    ).wait();
    loadMarketplaceItems();
  };

  useEffect(async () => {
    loadMarketplaceItems();
  }, []);

  // useEffect(() => {
  //   console.log(items);
  // }, [items]);



  return (
    <Container style={{minHeight: '420px'}} className={`${classes.bg_color}`}>
      <Row lg={12} md={12} xs={12}>
        {items.map((item, i) => (
          <Col lg={6} md={12} key={i}>
            <Product
              totalPrice={item.totalPrice}
              itemId={item.itemId}
              seller={item.seller}
              skinName={item.skinName}
              description={item.description}
              img={item.img}
              buyMarketItem={buyMarketItem}
              item={item}
            />
          </Col>
        ))}
      </Row>
    </Container>
  );
}

function Store({ marketplace, nft, web3Handler, account, loading }) {
  return (
    <Layout web3Handler={web3Handler} account={account}>
      {loading ? (
        <Spinner animation="border" variant="primary" />
      ) : (
        <Sub marketplace={marketplace} nft={nft} />
      )}
    </Layout>
  );
}

export default Store