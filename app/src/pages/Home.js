import React from "react";
import Layout from "../layouts/Layout";
import HomeOverview from "../components/HomeOverview";
import HomeIntroduction from "../components/HomeIntroduction/HomeIntroduction";
import HomeGuide from "../components/HomeGuide";
import {Link} from 'react-router-dom'
import {Spinner} from 'react-bootstrap'

function Home({ marketplace, nft, web3Handler, account, loading }) {
  let imgsrc = 'https://images2.alphacoders.com/720/thumbbig-720032.webp';
  let introimg =
    'https://www.pngmart.com/files/3/League-of-Legends-Logo-Transparent-Background.png';

  let imageItems = [
    { url: 'https://images5.alphacoders.com/719/thumbbig-719053.webp' },
    { url: 'https://images5.alphacoders.com/719/thumbbig-719053.webp' },
    { url: 'https://images5.alphacoders.com/719/thumbbig-719053.webp' },
    { url: 'https://images5.alphacoders.com/719/thumbbig-719053.webp' },
    { url: 'https://images5.alphacoders.com/719/thumbbig-719053.webp' },
  ];

  return (
    <Layout
      web3Handler={web3Handler}
      account={account}
      marketplace={marketplace}
    >
      {loading ? (
        <Spinner animation="border" variant="primary" />
      ) : (
        <>
          <HomeOverview
            imgsrc={imgsrc}
            marketplace={marketplace}
            account={account}
          />
          <HomeIntroduction introimg={introimg} />
          {/* <HomeGuide images={imageItems} /> */}
        </>
      )}
    </Layout>
  );
}

export default Home;
