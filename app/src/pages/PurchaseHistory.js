import React, { useState, useEffect } from "react";
import Layout from "../layouts/Layout";
import { CardGroup, Card, Button, Row, Col, Container, Modal, Spinner } from "react-bootstrap";
import { BigNumber, ethers } from "ethers";
import classes from "./PurchaseHistory.module.css";

function HistorySub({ marketplace, nft, account }) {
  const [loading, setLoading] = useState(true);
  const [purchases, setPurchases] = useState([]);
  const [code, setCode] = useState('');
  const [isShowCode, setIsShowCode] = useState(false);

  const toggleShowCode = () => setIsShowCode(!isShowCode);

  const loadPurchasedItems = async () => {
    const filter = marketplace.filters.Bought(
      null,
      null,
      null,
      null,
      null,
      account
    );
    const results = await marketplace.queryFilter(filter);
    
    const purchases = await Promise.all(
      results.map(async (i) => {
        i = i.args;
        const uri = await nft.tokenURI(i.tokenId);
        const response = await fetch(uri);
        const metadata = await response.json();
        const totalPrice = await marketplace.getTotalPrice(i.itemId);

        let purchasedItem = {
          price: totalPrice,
          itemId: i.itemId,
          skinName: metadata.skinName,
          description: metadata.description,
          img: metadata.img,
          tokenId: i.tokenId,
          // isActivated: metadata.isActivated,
        };
        return purchasedItem;
      })
    );
    setLoading(false);
    setPurchases(purchases);
  };
  useEffect(() => {
    loadPurchasedItems();
  }, []);

  async function showCodeHandler(tokenId) {
    const code = await nft.getCode(tokenId)
    setCode(code);
    toggleShowCode();
  }
  if(loading){
    return <Spinner animation="border" variant="primary" />
  }
  return (
    <Container>
      <Modal show={isShowCode} onHide={toggleShowCode}>
        <Modal.Header closeButton>
          <Modal.Title>Mã code của bạn</Modal.Title>
        </Modal.Header>
        <Modal.Body>{code}</Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={toggleShowCode}>
            Đóng
          </Button>
        </Modal.Footer>
      </Modal>
      
        {purchases.length > 0 ? (
          <div><Row xs={4} md={2} className="g-4">
            {purchases.map((item, index) => {
              return (
                
                <Col key={index} lg={6}>
                  <Card>
                    <Card.Img src={item.img} />
                    <Card.Body>
                      <Card.Title>{item.skinName}</Card.Title>
                      <Card.Text>
                        Giá{' '}
                        <span className={classes.price}>
                          {ethers.utils.formatEther(item.price)}{" "}ETH
                        </span>
                      </Card.Text>
                      {/* <Card.Text>
                        Mã code:{' '}
                        {isShowCode
                          ? ethers.utils.formatEther(item.code)
                          : '*********'}
                      </Card.Text>
                      <Card.Text>
                        Trạng thái: {ethers.utils.formatEther(item.isActivated)}
                      </Card.Text> */}
                    </Card.Body>
                    <Button
                      onClick={async () => {
                        await showCodeHandler(item.tokenId);
                      }}
                    >
                      Hiển thị code
                    </Button>
                  </Card>
                </Col>
              );
            })}</Row>
          </div>
        ) : (
          <h2>You have not made a purchase. </h2>
        )}
    </Container>
  );
}

function PurchaseHistory({ marketplace, nft, account, web3Handler, loading }) {

  return (
    <Layout web3Handler={web3Handler} account={account}>
      <h2 style={{color: "yellow", textAlign: "center", margin: "20px 0 40px"}}>Lịch sử mua hàng của bạn</h2>
      {loading ? (
        <Spinner animation="border" variant="primary" />
      ) : (
        <HistorySub marketplace={marketplace} nft={nft} account={account} />
      )}
    </Layout>
  );
}

export default PurchaseHistory;
