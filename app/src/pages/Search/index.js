import React, { useState, useEffect } from 'react'
import classes from "./Search.module.css"
import Layout from '../../layouts/Layout';
import { Container, Row, Col, ListGroup, Form, Accordion, Pagination } from 'react-bootstrap';
import Product from '../../components/Product';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faStar } from '@fortawesome/free-solid-svg-icons';
import img from '../../assets/images/Rectangle 273.png';
import { useStore, action } from '../../contexts/contractContext'
import { useParams } from 'react-router'

function Search({ marketplace, nft, web3Handler, account }) {
  const [state, dispatch] = useStore();
  const [type, setType] = useState('');
  const filter = useParams();
  const [filterItems, setFilterItems] = useState([]);
  const [items, setItems] = useState([]);
  const loadMarketplaceItems = async () => {
    // Load all unsold items
    const itemCount = await marketplace.itemCount();
    let items = [];
    for (let i = 1; i <= itemCount; i++) {
      const item = await marketplace.items(i);
      if (!item.sold) {
        // get uri url from nft contract
        const uri = await nft.tokenURI(item.tokenId);
        // use uri to fetch the nft metadata stored on ipfs
        const response = await fetch(uri);
        const metadata = await response.json();
        // get total price of item (item price + fee)
        const totalPrice = await marketplace.getTotalPrice(item.itemId);
        // Add item to items array
        items.push({
          totalPrice,
          itemId: item.itemId,
          seller: item.seller,
          chamName: metadata.chamName,
          skinName: metadata.skinName,
          description: metadata.description,
          img: metadata.img,
          year: metadata.year,
        });
      }
    }
    setItems(items);
  };

  // const buyMarketItem = async (item) => {
  //   await (
  //     await marketplace.purchaseItem(item.itemId, {
  //       value: item.totalPrice,
  //     })
  //   ).wait();
  //   loadMarketplaceItems();
  // };

  useEffect(async () => {
    loadMarketplaceItems();
  }, []);

  useEffect(() => {
    let temp = []

    items.map((item, i) => {
      const skinName = item.skinName.normalize('NFD').replace(/[\u0300-\u036f]/g, '').replace(/\s/g, '').toLowerCase()
      const searchInput = state.searchInput
      
      if (skinName.includes(searchInput)) {
        temp.push(item)
      }
    })

    setFilterItems(temp)
  }, [items]);

  useEffect(() => {
    switch (filter.type) {
      case 'mid':
        dispatch(action.setSearchInput(''));
        setType('Đường giữa');
        break;
      case 'jungle':
        dispatch(action.setSearchInput(''));
        setType('Rừng');
        break;
      case 'top':
        dispatch(action.setSearchInput(''));
        setType('Đường trên');
        break;
      case 'adc':
        dispatch(action.setSearchInput(''));
        setType('Xạ thủ');
        break;
      case 'support':
        dispatch(action.setSearchInput(''));
        setType('Hỗ trợ');
        break;
      default:
        setType('');
    }
  }, [filter]);

  return (
    <Layout web3Handler={web3Handler} account={account}>
      <Container className={`${classes.search}`}>
        {state.searchInput === '' ? (
          <Row>
            <h1 className={`${classes.search__title}`}>
              Tìm được {items.length} kết quả cho {type}
            </h1>
            <Col md={{ span: 6, offset: 3 }}>
              {items.map((item, i) => (
                <Product
                  totalPrice={item.totalPrice}
                  itemId={item.itemId}
                  seller={item.seller}
                  skinName={item.skinName}
                  description={item.description}
                  img={item.img}
                />
              ))}

              {/* <Pagination className={`${classes.search__pagination}`}>
                <Pagination.Prev />

                <Pagination.Item active>{1}</Pagination.Item>
                <Pagination.Item>{2}</Pagination.Item>
                <Pagination.Item>{3}</Pagination.Item>
                <Pagination.Item>{4}</Pagination.Item>
                <Pagination.Item>{5}</Pagination.Item>

                <Pagination.Next />
              </Pagination> */}
            </Col>
          </Row>
        ) : (
          <Row>
            <h1 className={`${classes.search__title}`}>
              Tìm được {filterItems.length} kết quả cho {state.searchInput}
            </h1>
            <Col md={{ span: 6, offset: 3 }}>
              {filterItems.map((item, i) => (
                <Product
                  totalPrice={item.totalPrice}
                  itemId={item.itemId}
                  seller={item.seller}
                  skinName={item.skinName}
                  description={item.description}
                  img={item.img}
                />
              ))}

              {/* <Pagination className={`${classes.search__pagination}`}>
                <Pagination.Prev />

                <Pagination.Item active>{1}</Pagination.Item>
                <Pagination.Item>{2}</Pagination.Item>
                <Pagination.Item>{3}</Pagination.Item>
                <Pagination.Item>{4}</Pagination.Item>
                <Pagination.Item>{5}</Pagination.Item>

                <Pagination.Next />
              </Pagination> */}
            </Col>
          </Row>
        )}
      </Container>
    </Layout>
  );
}

export default Search
