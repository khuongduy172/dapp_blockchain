import React from 'react';
import Product from '../components/Product';
import classes from './SellingItems.module.css';
import img from '../assets/images/Rectangle 273.png';


function SellingItems() {
  return (
    <div>
      <div style={{textAlign: "right"}}>
        <span className={classes.change_button} style={{marginRight: "10px"}}>+</span>
        <span className={classes.change_button}>-</span>
      </div>
    {Array(2).fill().map((_, i) => (
      <Product
        key={i}
        img={img}
        tag="Giá độc quyền"
        initialPrice="600000"
        discountPrice="150000"
        isFree={false}
        skinName="Skin Lee Sin"
        rating={5}
        isAttachedGift={false}
        isSoldOut={false}
        isCounted={true}
        isSelling={true}
      />
    ))}
    </div>
  )
}

export default SellingItems