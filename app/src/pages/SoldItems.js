import React from 'react';
import Product from '../components/Product';
import img from '../assets/images/Rectangle 273.png';
import { useParams } from 'react-router-dom';

function SoldItems() {

  let {items} = useParams();

  const soldItems = <div>
  {Array(2).fill().map((_, i) => (
    <Product
      key={i}
      img={img}
      tag="Giá độc quyền"
      initialPrice="600000"
      discountPrice="150000"
      isFree={false}
      skinName="Skin Lee Sin"
      rating={5}
      isAttachedGift={false}
      isSoldOut={false}
      isCounted={true}
      isSelling={true}
    />))    
  }
  </div>

  return (
    <div>
      {/*(() => {

        switch(items) {
          case 'sold':
            return soldItems;

          case 'selling':
            return soldItems;

          case 'my-store':
            return soldItems;    

          default:
        }
      })()*/}
      {soldItems}
    </div>
  )
}

export default SoldItems