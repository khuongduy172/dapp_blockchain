import React, {useEffect, useState} from "react";
import NavBavV2 from "../components/NavBar/NavBavV2";
import ManageTable from "../components/ManageTable";
import ManageSearch from "../components/ManageSearch";
import {Spinner} from 'react-bootstrap'
import { useHistory } from 'react-router-dom';

function ManageBoard({ marketplace, nft, web3Handler, account, loading }) {
  const history = useHistory()
  useEffect(async () => {
    if(!account) {
      history.push('/home')
    } else {
      const acc = await marketplace.feeAccount();
      if(account.toLowerCase() !== acc.toLowerCase()) {
        history.push('/home')
      }
    }
  }, [account])
  return (
    <>
      <NavBavV2 web3Handler={web3Handler} account={account} />
      {loading ? (<Spinner animation="border" variant="primary" />):(
        <>
          <ManageSearch />
          <ManageTable marketplace={marketplace} nft={nft}/>
        </>
      )}
    </>
  );
}

export default ManageBoard;
