import React, { useState, useEffect } from "react";
import { Switch, Route, Redirect } from "react-router-dom";
import Home from "../pages/Home";
import Store from "../pages/Store";
import ProductDetail from "../pages/ProductDetail";
import ManageBoard from "../pages/ManageBoard";
import Marketplace from "../pages/Marketplace";
import ProductManagement from "../pages/ProductManagement";
import Search from "../pages/Search";
import Cart from "../pages/Cart";
import NotFound404 from "../pages/NotFound404";

import { ethers } from "ethers";
import NFTAbi from "../contracts/NFT.json";
import MarketplaceAbi from "../contracts/Marketplace.json";
import PurchaseHistory from "../pages/PurchaseHistory";

function PageRoutes() {
  const [account, setAccount] = useState(null);
  const [nft, setNFT] = useState({});
  const [marketplace, setMarketplace] = useState({});
  const [loading, setLoading] = useState(true);
  // MetaMask Login/Connect
  const web3Handler = async () => {
    const accounts = await window.ethereum.request({
      method: "eth_requestAccounts",
    });
    setAccount(accounts[0]);
    // Get provider from Metamask
    const provider = new ethers.providers.Web3Provider(window.ethereum);
    // Set signer
    const signer = provider.getSigner();

    window.ethereum.on("chainChanged", (chainId) => {
      window.location.reload();
    });

    window.ethereum.on("accountsChanged", async function (accounts) {
      setAccount(accounts[0]);
      await web3Handler();
    });
    loadContracts(signer);
  };
  const loadContracts = async (signer) => {
    // Get deployed copies of contracts
    const marketplace = new ethers.Contract(
      MarketplaceAbi.networks[5777].address,
      MarketplaceAbi.abi,
      signer
    );
    setMarketplace(marketplace);
    const nft = new ethers.Contract(
      NFTAbi.networks[5777].address,
      NFTAbi.abi,
      signer
    );
    setNFT(nft);
    setLoading(false);
  };
  useEffect(async () => {
    // Get provider from Metamask
    const provider = new ethers.providers.Web3Provider(window.ethereum);
    // Set signer
    const signer = provider.getSigner();
    loadContracts(signer);
  }, []);
  return (
    <Switch>
      <Route path="/" exact>
        <Redirect to="/home" />
      </Route>
      <Route path="/home" exact>
        <Home
          marketplace={marketplace}
          nft={nft}
          web3Handler={web3Handler}
          account={account}
        />
      </Route>
      <Route path="/store" exact>
        <Store
          marketplace={marketplace}
          nft={nft}
          web3Handler={web3Handler}
          account={account}
          loading={loading}
        />
      </Route>
      <Route path="/marketplace" exact>
        <Marketplace
          marketplace={marketplace}
          nft={nft}
          web3Handler={web3Handler}
          account={account}
        />
      </Route>
      <Route path="/cart" exact>
        <Cart
          marketplace={marketplace}
          nft={nft}
          web3Handler={web3Handler}
          account={account}
          loading={loading}
        />
      </Route>
      <Route path="/store/:skinID" exact>
        <ProductDetail
          marketplace={marketplace}
          nft={nft}
          web3Handler={web3Handler}
          account={account}
          loading={loading}
        />
      </Route>
      <Route path="/search" exact>
        <Search
          marketplace={marketplace}
          nft={nft}
          web3Handler={web3Handler}
          account={account}
        />
      </Route>
      <Route path="/search/:type" exact>
        <Search
          marketplace={marketplace}
          nft={nft}
          web3Handler={web3Handler}
          account={account}
        />
      </Route>
      <Route path="/product-management" exact>
        <ProductManagement
          marketplace={marketplace}
          nft={nft}
          web3Handler={web3Handler}
          account={account}
          loading={loading}
        />
      </Route>
      <Route path="/manage-board" exact>
        <ManageBoard
          marketplace={marketplace}
          nft={nft}
          web3Handler={web3Handler}
          account={account}
          loading={loading}
        />
      </Route>

      <Route path="/purchase-history">
        <PurchaseHistory
          marketplace={marketplace}
          nft={nft}
          web3Handler={web3Handler}
          account={account}
          loading={loading}
        />
      </Route>
      <Route path="*">
        <NotFound404 />
      </Route>
    </Switch>
  );
}

export default PageRoutes;
