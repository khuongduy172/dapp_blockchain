import React from 'react';
import NavBavV2 from '../components/NavBar/NavBavV2';

function Layout(props) {
  return (
    <>
      <header>
        <NavBavV2 web3Handler={props.web3Handler} account={props.account} />
      </header>
      <main>{props.children}</main>
    </>
  );
}

export default Layout;
