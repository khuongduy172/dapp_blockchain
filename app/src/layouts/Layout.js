import React from "react";
import Footer from "../components/Footer/Footer";
import NavBar from "../components/NavBar/NavBar";
import classes from './Layout.module.css'

function Layout({children, web3Handler, account, marketplace}) {
  return (
    <div className={classes.root__wrapper}>
      <header>
        <NavBar web3Handler={web3Handler} account={account} marketplace={marketplace}/>
      </header>
      <main style={{ backgroundColor: "#012641", padding: "20px 0"}}>{children}</main>
      <footer>
        <Footer />
      </footer>
    </div>
  );
}

export default Layout;
