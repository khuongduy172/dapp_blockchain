import { create as ipfsHttpClient } from 'ipfs-http-client';
export const client = ipfsHttpClient('/ip4/127.0.0.1/tcp/5001');
// export const client = ipfsHttpClient('https://ipfs.infura.io:5001/api/v0');
