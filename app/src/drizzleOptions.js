import Web3 from "web3";
import NFT from "./contracts/NFT.json";
import Marketplace from './contracts/Marketplace.json';

const options = {
  web3: {
    block: false,
    customProvider: new Web3("ws://localhost:8545"),
  },
  contracts: [NFT, Marketplace],
  // events: {
  //   SimpleStorage: ["StorageSet"],
  // },
};

export default options;
