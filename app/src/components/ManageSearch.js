import React, { useState } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faSearch } from "@fortawesome/free-solid-svg-icons";
import classes from "./ManageSearch.module.css";
import { Button } from "react-bootstrap";
import { Link, useHistory } from "react-router-dom";

function ManageSearch() {
  const [param, setParam] = useState("");
  const history = useHistory()
  function paramChangeHandler(e) {
    setParam(e.target.value.trim());
  }

  function newPostClickHandler() {
    history.push('/product-management');
  }

  return (
    <>
    <div className={classes.searchbar}>
      <input
        className={classes["search-input"]}
        placeholder="Tìm kiếm"
        onChange={paramChangeHandler}
      />
      <button className={classes.button}>
        <FontAwesomeIcon className={classes.icon}  icon={faSearch} />
      </button>
      <Button style={{fontSize: "1.4rem"}} onClick={newPostClickHandler}>Đăng bài</Button>
    </div>
    </>
  );
}

export default ManageSearch;
