import React, {useContext, useEffect} from 'react';
import Card from 'react-bootstrap/Card';
import Button from 'react-bootstrap/Button';
import { Row, Col, Container } from 'react-bootstrap';
import { Badge } from 'react-bootstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faStar } from '@fortawesome/free-solid-svg-icons';
import { faCartShopping } from '@fortawesome/free-solid-svg-icons';
import classes from './Product.module.css';
import {useCartStore, action} from '../contexts/cartContext';
import {BigNumber, ethers} from 'ethers'
import { Link } from 'react-router-dom';

function Product({
  totalPrice,
  itemId,
  seller,
  skinName,
  description,
  img,
  buyMarketItem,
  item,
}) {
  const handleAddItem = () => {
    let cart = localStorage.getItem('cart')
      ? JSON.parse(localStorage.getItem('cart'))
      : [];
    let flag = false;
    for(let i =0;i<cart.length;i++){
      let a = BigNumber.from(cart[i].itemId);
      let b = BigNumber.from(itemId);
      if(a.eq(b)){
        flag = true;
      }
    }
    if (flag == false) {
      cart.push({
        totalPrice: totalPrice,
        itemId: itemId,
        seller: seller,
        skinName: skinName,
        description: description,
        img: img,
      });
      localStorage.setItem('cart', JSON.stringify(cart));
    }

  };

  const handleAfterBuyItem = (item) => {
    let cart = localStorage.getItem('cart')
      ? JSON.parse(localStorage.getItem('cart'))
      : [];
      for (let i = 0; i < cart.length; i++) {
        let a = BigNumber.from(cart[i].itemId);
        let b = BigNumber.from(item.itemId);
        if(a.eq(b)){
          cart.splice(i, 1)
          console.log(cart);
      }
    }

    localStorage.setItem('cart', JSON.stringify(cart));
  }

  return (
    <Card className={classes.card_style}>
      <Row>
        <Col lg={6} md={5} sm={12}>
          <Card.Img
            variant="top"
            src={img}
            className={`${classes.img_width}`}
          />
        </Col>
        <Col lg={6} md={7} sm={12}>
          <Card.Body style={{ padding: '0' }}>
            <Row>
              <Col className={`${classes.col_style}`}>
                <Card.Title>
                  <div>
                    <Link to={`/store/${itemId}`}>{skinName}</Link>
                  </div>
                  <p className={`${classes.color_2} ${classes.font_20}`}>
                    {ethers.utils.formatEther(totalPrice)}{" "}ETH
                  </p>
                </Card.Title>
                <div>
                  <div className={classes.flex_between}>
                    <FontAwesomeIcon
                      icon={faCartShopping}
                      style={{ marginRight: '10px' }}
                      className={classes.cart_icon}
                      onClick={handleAddItem}
                    />
                    <Button variant="danger" onClick={async () => { await buyMarketItem(item); handleAfterBuyItem(item)}}>Mua ngay</Button>
                  </div>
                </div>
              </Col>
            </Row>
          </Card.Body>
        </Col>
      </Row>
    </Card>
  );
}

export default Product