import React from "react";
import classes from "./HomeGuide.module.css";
import { Carousel } from "react-bootstrap";

function HomeGuide(props) {
  let steps = [
    {
      index: 1,
      title: "Tìm kiếm",
      description: "Lựa chọn sản phẩm yêu thích",
      imgURL: props.images,
    },
    {
      index: 2,
      title: "Thêm vào giỏ hàng",
      description: "Thêm sản phẩm ưa thích vào giỏ hàng",
      imgURL: props.images,
    },
    {
      index: 3,
      title: "Thanh toán",
      description: "Thêm sản phẩm ưa thích vào giỏ hàng",
      imgURL: props.images,
    },
  ];
  return (
    <section className={classes.container}>
      <div className={classes["vertical-line"]} />
      <h2>GIAO DỊCH ĐƠN GIẢN, AN TOÀN</h2>
      <Carousel prevIcon={""}>
        {steps.map((step) => {
          return (
            <Carousel.Item key={step.index}>
              <div className={classes.step}>
                <h3>{step.index}</h3>
                <div className={classes["image-container"]}>
                  {step.imgURL.map((item) => {
                    return <img className={classes.img} src={item.url} />;
                  })}
                </div>
              </div>
              <Carousel.Caption className={classes.caption}>
                <h4>{step.title}</h4>
                <p>{step.description}</p>
              </Carousel.Caption>
            </Carousel.Item>
          );
        })}
      </Carousel>
    </section>
  );
}

export default HomeGuide;
