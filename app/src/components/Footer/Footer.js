import React from "react";
import classes from "./Footer.module.css";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faFacebookSquare,
  faTwitterSquare,
  faInstagramSquare,
  faWhatsappSquare,
} from "@fortawesome/free-brands-svg-icons";
import { faEnvelope, faPhoneAlt } from "@fortawesome/free-solid-svg-icons";
import { faCopyright } from "@fortawesome/free-regular-svg-icons";
import { Link } from "react-router-dom";

function Footer() {
  return (
    <div className={classes["footer-bg"]}>
      <p className={classes.logo}>LOLMarketplace</p>
      <section className={classes.info}>
        <div>
          <p>Kết nối với chúng tôi</p>
          <div className={classes.brands}>
            <FontAwesomeIcon icon={faFacebookSquare} />
            <FontAwesomeIcon icon={faTwitterSquare} />
            <FontAwesomeIcon icon={faInstagramSquare} />
            <FontAwesomeIcon icon={faWhatsappSquare} />
          </div>
        </div>
        <div>
          <p style={{marginBottom: "0px"}}>Liên hệ</p>
          <address>
            <FontAwesomeIcon style={{marginRight: "1rem"}} icon={faEnvelope} />
            support@lolmarketplace.com
            <br />
            <FontAwesomeIcon style={{marginRight: "1rem"}} icon={faPhoneAlt} />
            1900 1234
            <br />
          </address>
        </div>
        <div className={classes.others}>
          <Link to="">Điều khoản dịch vụ</Link>
          <Link to="">Chính sách bảo mật</Link>
          <p>
            <FontAwesomeIcon icon={faCopyright} /> LOL Marketplace, Inc
          </p>
        </div>
      </section>
    </div>
  );
}

export default Footer;
