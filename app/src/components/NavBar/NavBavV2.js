import React from "react";
import { Navbar, Nav, Button } from "react-bootstrap";
import classes from "./NavBarV2.module.css";
import {Link} from 'react-router-dom'

function NavBavV2({web3Handler, account}) {
  return (
    <Navbar expand="lg" className={classes['nav-bg']}>
      <Navbar.Brand className={classes['brand-name']} as={Link} to={'/home'}>
        LOLMarketplace
      </Navbar.Brand>
      <Nav className={classes.nav}>
        <Nav.Item>
          <Nav.Link className={classes.header} as={Link} to="/manage-board">
            Quản lý
          </Nav.Link>
        </Nav.Item>
        <Nav.Item>
          <Nav.Link
            className={classes.header}
            as={Link}
            to="/product-management"
          >
            Tạo sản phẩm
          </Nav.Link>
        </Nav.Item>
      </Nav>
      <Nav>
        {account ? (
          <Nav.Link
            href={`https://etherscan.io/address/${account}`}
            target="_blank"
            rel="noopener noreferrer"
            className="button nav-button btn-sm mx-4"
          >
            <Button variant="outline-light">
              {account.slice(0, 5) + '...' + account.slice(38, 42)}
            </Button>
          </Nav.Link>
        ) : (
          <Button onClick={web3Handler} variant="outline-light">
            Kết nối Metamask
          </Button>
        )}
      </Nav>
    </Navbar>
  );
}

export default NavBavV2;
