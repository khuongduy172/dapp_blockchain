import React, { useState } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faSearch } from "@fortawesome/free-solid-svg-icons";
import { Link } from "react-router-dom";
import { DropdownButton } from "react-bootstrap";
import "bootstrap/dist/css/bootstrap.css";
import DropdownItem from "react-bootstrap/esm/DropdownItem";
import classes from "./SearchBar.module.css";
import { useStore, action } from '../../contexts/contractContext'

function SearchBar() {
  const [state, dispatch] = useStore()
  const [search, setSearch] = useState()

  function handleSearchInput(e) {
    dispatch(action.setSearchInput(search))
  }

  return (
    <div className={classes.searchbar}>
      <input
        className={classes["search-input"]}
        placeholder='Tìm kiếm'
        onChange={e => setSearch(e.target.value)}
      />
      <Link className={classes["search-icon"]} to={`/search`} onClick={handleSearchInput}>
        <FontAwesomeIcon icon={faSearch} />
      </Link>
      <DropdownButton className={classes.filter} title='Lọc theo loại tướng'>
        <DropdownItem className={classes["filter-item"]}>
          <Link to='/search/mid'>
            Đường giữa
          </Link>
        </DropdownItem>

        <DropdownItem className={classes["filter-item"]}>
          <Link to='/search/jungle'>Rừng</Link>
        </DropdownItem>
        <DropdownItem className={classes["filter-item"]}>
          <Link to='/search/top'>Đường trên</Link>
        </DropdownItem>
        <DropdownItem className={classes["filter-item"]}>
          <Link to='/search/adc'>Xạ thủ</Link>
        </DropdownItem>
        <DropdownItem className={classes["filter-item"]}>
          <Link to='/search/support'>Hỗ trợ</Link>
        </DropdownItem>
      </DropdownButton>
    </div>
  );
}

export default SearchBar;
