import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import React, { useEffect, useState } from "react";
import SearchBar from "./SearchBar";
import { Navbar, Nav, Button, Dropdown } from "react-bootstrap";
import { faCartShopping } from "@fortawesome/free-solid-svg-icons";
import classes from "./NavBar.module.css";
import "bootstrap/dist/css/bootstrap.css";
import { Link } from "react-router-dom";

function NavBar({ web3Handler, account }) {
  useEffect(async () => {
    await web3Handler();
  },[])

  const [count, setCount] = useState(0);

  useEffect(() => {
    let cart = localStorage.getItem("cart") ? JSON.parse(localStorage.getItem("cart")) : [];
    setCount(cart.length);
  }, [JSON.parse(localStorage.getItem("cart"))?.length]);

  return (
    <Navbar expand="lg" className={classes["nav-bg"]}>
      <Navbar.Brand className={classes["brand-name"]}>
        <Nav.Link as={Link} to="/home">
          LOLMarketplace
        </Nav.Link>
      </Navbar.Brand>
      <Nav
        defaultActiveKey={"/home"}
        className={`${classes.nav} justify-content-evenly`}
      >
        <Nav.Item>
          <Nav.Link className="text-white" as={Link} to="/home">
            Trang Chủ
          </Nav.Link>
        </Nav.Item>
        <Nav.Item>
          <Nav.Link className="text-white" as={Link} to="/store">
            Cửa Hàng
          </Nav.Link>
        </Nav.Item>
        <Nav.Item>
          <SearchBar />
        </Nav.Item>
        <Nav.Item style={{position: "relative"}}>
          <Nav.Link className={classes.cart} as={Link} to="/cart">
            Giỏ Hàng <FontAwesomeIcon icon={faCartShopping} />
          </Nav.Link>
          {count > 0 ? <p title={count} className={classes.countItem}>{count}</p> : ""}
        </Nav.Item>
        <Nav>
          {account ? (
            <Dropdown>
              <Dropdown.Toggle variant="success" id="dropdown-basic">
                {account.slice(0, 5) + "..." + account.slice(38, 42)}
              </Dropdown.Toggle>

              <Dropdown.Menu>
                <Dropdown.Item href="/purchase-history">Lịch sử mua hàng</Dropdown.Item>
              </Dropdown.Menu>
            </Dropdown>
          ) : (
            <Button onClick={web3Handler} variant="outline-light">
              Kết nối Metamask
            </Button>
          )}
        </Nav>
      </Nav>
    </Navbar>
  );
}

export default NavBar;
