import React, {useEffect, useState} from "react";
import { Link } from "react-router-dom";
import classes from "./HomeOverview.module.css";

function HomeOverview(props) {
  const [admin, setAdmin] = useState(true)
  // useEffect(async () => {
  //   if (props.account) {
  //     const acc = await props.marketplace.feeAccount();
  //     if (props.account.toLowerCase() !== acc.toLowerCase()) {
  //       setAdmin(true)
  //     }
  //   }
  // }, [props.account]);
  // useEffect(()=> {
  //   console.log(admin);
  // },[admin])
  return (
    <section className={classes.container}>
      {admin && (
        <Link to={'/manage-board'}>Admin</Link>
      )}
      <div className={classes.intro}>
        <h1>THỊ TRƯỜNG NFT HÀNG ĐẦU CHO GAMERS</h1>
        <p>Trao đổi vật phẩm an toàn, tin cậy</p>
        <Link to="/store">Khám phá ngay</Link>
      </div>
      <div className={classes['img-container']}>
        <img
          className={classes.image}
          src={props.imgsrc}
          alt="Vayne đoạt hồn"
        />
      </div>
    </section>
  );
}

export default HomeOverview;
