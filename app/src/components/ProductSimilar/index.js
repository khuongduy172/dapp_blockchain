import React, { forwardRef } from 'react'
import productSimilar from './ProductSimilar.module.css'

function ProductSimilar({index, name, price, image, state }, ref) {
  return (
    <li key={index} className={productSimilar.product_similar__item} ref={ref}>
      <div className={productSimilar.product_similar__item_image}>
        <img className={productSimilar.product_similar__item_image__item} src={image} alt="img" />
      </div>

      <div className={productSimilar.product_similar__item_name}>{name}</div>

      <div className={productSimilar.product_similar__item_state}>Tình trạng: {state}</div>

      <div className={productSimilar.product_similar__item_price}>{price} ETH</div>
    </li>
  )
}

export default forwardRef(ProductSimilar)