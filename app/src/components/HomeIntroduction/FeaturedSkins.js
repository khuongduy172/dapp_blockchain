import React from "react";
import classes from "./FeaturedSkins.module.css";
import { Link } from "react-router-dom";

function FeaturedSkins(props) {
  return (
    <div className={classes.container}>
      <h3>Một số nhóm trang phục nổi bật</h3>
      <ul className={classes.content}>
        {props.categories.map((item) => {
          return (
            <li className={classes.item}>
              <Link to="/">{item.category}</Link>
            </li>
          );
        })}
      </ul>
    </div>
  );
}

export default FeaturedSkins;
