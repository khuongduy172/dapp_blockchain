import React from "react";
import classes from "./ImageCarousel.module.css";
import { Carousel } from "react-bootstrap";

function ImageCarousel(props) {
  return (
    <div>
      <Carousel interval={4000}>
        {props.items.map((item) => {
          return (
            <Carousel.Item >
              <img className={classes.image} src={item.url}/>
            </Carousel.Item>
          );
        })}
      </Carousel>
    </div>
  );
}

export default ImageCarousel;
