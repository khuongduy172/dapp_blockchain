import React from "react";
import classes from "./HomeIntroduction.module.css";
import FeaturedSkins from "./FeaturedSkins";
import ImageCarousel from "./ImageCarousel";

function HomeIntroduction(props) {
  let skincategories = [
    { category: "Siêu phẩm" },
    { category: "Tối thượng" },
    { category: "Quán quân" },
    { category: "Giả lập" },
    { category: "Hắc tinh" },
  ];

  let imageItems = [
    { url: "https://images5.alphacoders.com/719/thumbbig-719053.webp" },
    { url: "https://images8.alphacoders.com/844/thumbbig-844659.webp" },
    { url: "https://images2.alphacoders.com/627/thumbbig-627081.webp" },
  ];
  return (
    <section className={classes.container}>
      <div className={classes["vertical-line"]} />
      <h2>GIỚI THIỆU</h2>
      <div className={classes.intro}>
        <p>
          <span className={classes.icon}>LOLMarketplace</span> là một nền tảng
          đem đến các trải nghiệm tuyệt vời, an toàn, đáng tin cậy trong giao
          dịch các trang phục của nhân vật trong tựa game nổi tiếng - League of
          Legend. Nhờ áp dụng công nghệ Blockchain, giờ đây việc mua hàng trở
          nên dễ dàng, nhanh chóng mà an toàn hơn bao giờ hết.
        </p>
        <img className={classes.img} src={props.introimg} />
      </div>
      <FeaturedSkins categories={skincategories} />
      <ImageCarousel items={imageItems} />
    </section>
  );
}

export default HomeIntroduction;
