import React, { useRef, useState, useEffect } from "react";
import classes from "./ManageTable.module.css";
import { Table, Pagination } from "react-bootstrap";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faPenToSquare, faTrash } from "@fortawesome/free-solid-svg-icons";
import {ethers} from 'ethers'

function ManageTable({marketplace, nft}) {
  
  function addElipsis(text) {
    return text.substring(0, 70) + "...";
  }

  // useEffect(() => {
  //   let pages = Math.ceil(fakeData.length / 10);
  //   console.log(pages);
  //   let active = 1;
  //   let totalPages = [];
  //   for (let index = 1; index <= pages; index++) {
  //     totalPages.push(
  //       <Pagination.Item key={index} active={index === active}>
  //         {index}
  //       </Pagination.Item>
  //     );
  //   }
  // }, []);

  const [items, setItems] = useState([]);
  const loadMarketplaceItems = async () => {
    // Load all unsold items
    const itemCount = await marketplace.itemCount();
    let items = [];
    for (let i = 1; i <= itemCount; i++) {
      const item = await marketplace.items(i);
      if (true) {
        // get uri url from nft contract
        const uri = await nft.tokenURI(item.tokenId);
        // use uri to fetch the nft metadata stored on ipfs
        const response = await fetch(uri);
        const metadata = await response.json();
        // get total price of item (item price + fee)
        const totalPrice = await marketplace.getTotalPrice(item.itemId);
        const active = await nft.active(item.tokenId);
        // Add item to items array
        items.push({
          totalPrice,
          itemId: item.itemId,
          tokenId: item.tokenId,
          seller: item.seller,
          chamName: metadata.chamName,
          skinName: metadata.skinName,
          description: metadata.description,
          img: metadata.img,
          year: metadata.year,
          sold: item.sold,
          status: active
        });
      }
    }
    setItems(items);
  };

  useEffect(async () => {
    loadMarketplaceItems();
  }, []);

  const handleDelete = async(tokenId, status)=> {
    try {
      await nft.updateStatusAdmin(tokenId, !status);
      loadMarketplaceItems();
    } catch (error) {
      alert("Thao tác thất bại")
      console.log(error);
    }
  }

  return (
    <>
      <Table className={classes.table} striped bordered hover>
        <thead>
          <tr>
            <th>ID</th>
            <th>Tên trang phục</th>
            <th>
              Giá (Đơn vị:<span style={{ color: "red" }}> ETH</span>)
            </th>
            <th>Năm phát hành</th>
            <th>Mô tả</th>
            <th>Tuỳ chọn</th>
          </tr>
        </thead>
        <tbody>
          {items.length > 0 && items.map((row, index) => {
            return (
              <tr key={index}>
                <td>{index}</td>
                <td>{row.chamName}</td>
                <td>{ethers.utils.formatEther(row.totalPrice)}</td>
                <td>{row.year}</td>
                <td className={classes.description}>
                  {addElipsis(row.description)}
                </td>
                <td>
                  <div className={classes.buttons}>
                    <button className={classes.delete}>
                      <FontAwesomeIcon icon={!row.status ? faTrash : faPenToSquare} size="lg" onClick={()=> {handleDelete(row.tokenId, row.status)}}/>
                    </button>
                  </div>
                </td>
              </tr>
            );
          })}
        </tbody>
      </Table>
    </>
  );
}

export default ManageTable;
