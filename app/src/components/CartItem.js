import React, { useEffect } from 'react';
import classes from './CartItem.module.css';
import Card from 'react-bootstrap/Card';
import Form from 'react-bootstrap/Form';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Button from 'react-bootstrap/Button';
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faTrashCan } from "@fortawesome/free-solid-svg-icons";
import img from '../assets/images/Rectangle 273.png';
import {ethers} from 'ethers'

function CartItem({skinName, totalPrice, itemId, seller, img, number, marketplace, item}) {

  const handleRemoveCartItem = (id) => {
    let cart = JSON.parse(localStorage.getItem("cart"));
  
    cart.splice(number, 1)
  
    localStorage.setItem("cart", JSON.stringify(cart));
    window.location.reload();
  }

  const buyMarketItem = async (item) => {
    await (
      await marketplace.purchaseItem(item.itemId, {
        value: item.totalPrice,
      })
    ).wait();
  };

  return (
    <Card className={classes.flex_row} style={{marginBottom: "20px",padding: "10px"}}>
      <Form.Check 
        type="checkbox"
        id={itemId.hex}
        label=""
      />
      <Card.Img variant="top" src={img} style={{width: "50%"}}/>
      <Card.Body className={classes.flex_column} style={{padding: "0 20px"}}>
        <Card.Title>{skinName}</Card.Title>
        <Card.Text>
          <span>Giá: <b style={{color: "orange"}}>{ethers.utils.formatEther(totalPrice)} ETH</b></span><br />
        </Card.Text>
        <Row>
          <Col><Button variant="dark" style={{width: "120px", height: "40px"}} onClick={() => handleRemoveCartItem(number)}><FontAwesomeIcon icon={faTrashCan} /></Button></Col>
          <Col><Button variant="danger" onClick={async() => {await buyMarketItem(item); handleRemoveCartItem(number)}}>Mua ngay</Button></Col>
        </Row>
        
      </Card.Body>
    </Card>
  )
}

export default CartItem