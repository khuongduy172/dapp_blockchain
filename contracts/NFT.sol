// SPDX-License-Identifier: MIT
pragma solidity ^0.8.12;

import "@openzeppelin/contracts/token/ERC721/extensions/ERC721URIStorage.sol";
import "@openzeppelin/contracts/access/Ownable.sol";

contract NFT is ERC721URIStorage, Ownable {
    uint public tokenCount;
    mapping(uint => string) code;
    mapping(uint => bool) public active;
    constructor() ERC721("LOL Item NFT", "LOL"){}
    function mint(string memory _tokenURI, string memory _code) external returns(uint) {
        tokenCount ++;
        code[tokenCount] = _code;
        active[tokenCount] = false;
        _safeMint(msg.sender, tokenCount);
        _setTokenURI(tokenCount, _tokenURI);
        return(tokenCount); 
    }

    function getCode(uint _tokenId) public view returns(string memory) {
        require(this.ownerOf(_tokenId) == msg.sender, "you are not owner");
        return code[_tokenId];
    }

    function updateStatus (string memory _code) public returns (bool) {
        // Get address caller
        address _from = msg.sender;
        uint _tokenId = 0;
        
        // Find tokenId
        for (uint i = 1; i <=tokenCount; i++) {
            // keccak256(abi.encodePacked()) to compare string
            if(keccak256(abi.encodePacked(code[i])) == keccak256(abi.encodePacked(_code))) {
                // Assign _tokenId if find
                _tokenId = i;
                break;
            }
        }

        // Check owner of _tokenId
        require(this.ownerOf(_tokenId) == _from, "You don't own this NFT");

        /*  Check active status, if false => true and respone admin 'true', otherwise respone 'false';
            Controlling in admin we don't care, responsibility only notify with admin value 'true' or 'false' with 
            each activing code of player */
        if(_tokenId > 0 && active[_tokenId] == false) {
            active[_tokenId] = true;
        }
        return (active[_tokenId]);
    }
    function updateStatusAdmin (uint _tokenId, bool _status) public onlyOwner {
        active[_tokenId] = _status;
    }
}
