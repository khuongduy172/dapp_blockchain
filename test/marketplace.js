const { assert } = require("chai");
const truffleAssert = require('truffle-assertions');

const Marketplace = artifacts.require("Marketplace");

/*
 * uncomment accounts to access the test accounts made available by the
 * Ethereum client
 * See docs: https://www.trufflesuite.com/docs/truffle/testing/writing-tests-in-javascript
 */
contract("Marketplace", function (accounts) {
  it("should return the list of accounts", async ()=> {
    // console.log(accounts);
  });

  it("should deployed", async function () {
    let instance = await Marketplace.deployed(5);
    instance = Boolean(instance);
    return assert.isTrue(instance);
  });

  it("should check feePercent", async () => {
    let instance = await Marketplace.deployed(5);
    return assert.equal(await instance.feePercent(), 5);
  })

  it("should count item", async function () {
    let instance = await Marketplace.deployed();
    let count = await instance.itemCount();
    return assert.equal(count, 0);
  });

  
});
